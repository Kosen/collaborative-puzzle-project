﻿Collaborative puzzle made by Panagiota Tziova & Remi Cambuzat (2015)
Groupware and Collaborative Interaction course - Université Paris-Sud ( Michel Beaudouin-Lafon, Cédric Fleury)


Freely inspired by  Marcelo Ricardo de Oliveira tutorial (Html5 Jigsaw Puzzle) :

                       http://www.codeproject.com/Articles/395453/Html-Jigsaw-Puzzle

And from those sources from sney2002 on codecanyon:

		http://codecanyon.net/item/canvas-puzzle/2289964

Technology : HTML5, paper.js , Node.js + socket.io

Browser side :

    Require jquery-2.1.3.min.js and paper.js to work

Server side :

    Require Node.js
    Require aditional package : express + socket.io
        To install those package run "npm install <package_name>" this will create a folder "node_modules/<package_name>"
	As this project contain a package.json just run "npm install"

    <NOT IMPLEMENTED YET>Require to install shareJs

Files in the repository :
	
	/old/* : unused
	/newPuzzle/* : on going developments based on a lib
	/simplePuzzle/* : from scratch development


To run the solution in localhost: 

	node <server_name>

	server.js : simple puzzle application
	serverNew.js : new puzzle application
	serverOld.js : old puzzle

Online version:

	http://www.kosen.me/
	