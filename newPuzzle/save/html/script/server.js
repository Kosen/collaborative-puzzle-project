/**
 * Created by kosen on 15/02/2015.
 */
var client = null;	// socket connection to server


function connect() {
    if (client)
        return;	// already connected
    $('#chat').empty();
    $('#chat').append('<p>Connecting to server</p>');
    client = io();

    // Handler when connected
    client.on('connect', function() {
        $('#chat').empty();
        $('#chat').append('<p>Connected to server</p>');
        // Say hi
        client.emit('hello', $('#name').val() || 'unknown');
        $('#name').prop( "disabled", true ); //desactivate the form one the once connected
        $('#connect_button').hide(); // Hide the connect button
    });

    // Handler when lost connection
    client.on('disconnect', function() {
        /*$('#chat').empty();
        $('#chat').append('<p>Disconnected from server</p>');*/
    });

    /*client.on('settingmsg', function(data) {
        //setPuzzleSettings(data);
        //setPuzzleSettings(data);
        puzzle_setting.USER_COLOR = data.user_color;
        $('#name').css('color', puzzle_setting.USER_COLOR);
        console.log("USER_COLOR : "+ puzzle_setting.USER_COLOR);
    });*/

    // Handler when user joins
    client.on('hello', function(name) {
        $('#chat').empty();
        $('#chat').append('<p style="color:green">'+name+' joined</p>');
    });

    // Handler when user sends message
    client.on('msg', function(data) {
        $('#chat').empty();
        $('#chat').append('<p>'+data.from+': '+data.msg+'</p>');
    });

    //Handler use when the parameter of a tile should be updated
    client.on('updateTiles', function(tile) {
        console.log("update tile (client side)");
        //updateTile(tile);
    });

    // Handler when user leaves
    client.on('bye', function(name) {
        $('#chat').empty();
        $('#chat').append('<p style="color:red">'+name+' has left</p>');
    });
}

// Send a message
function sendMessage() {
    if (client) {
        client.emit('msg', {
            from: $('#name').val(),
            msg: $('#msg').val()
        });
        $('#chat').empty();
        $('#chat').append('<p style="color:blue">'+$('#msg').val()+'</p>');
    }
}

function sendTile(tile)
{
    console.log('send tile (client side)')
    if (client) {
        client.emit('updateTiles', tile);
    }
}
