/**
 * Created by kosen on 15/02/2015.
 */
// Create Express app
var express = require('express');
var app = express();

// Get the http server created by Express
var http = require('http').Server(app);
// Load socket.io
var io = require('socket.io')(http);

// Serve /
app.get('/', function (req, res) {
    res.sendFile(__dirname + "/newPuzzle/html/index.html");
});

// Serve static pages from the html directory
app.use('/', express.static(__dirname + '/newPuzzle/html'));

// Listen to socket connections
io.on('connect', function(client) {
    //console.log('A user connected');

    var user = 'unknown';

    // Listen to disconnections
    client.on('disconnect', function() {
        console.log(user, 'disconnected');
        io.emit('bye', user);
    });

    // Listen to client messages
    client.on('hello', function(name) {
        user = name;
        console.log(user, 'joined');

        // Welcomme message
        client.emit('msg',{
            from: 'Serveur',
            msg: "Bonjour " + user });

        // Broadcast to others
        client.broadcast.emit('hello', name);
    });
    client.on('msg', function(data) {
        console.log(data);
        // Broadcast to others
        client.broadcast.emit('msg', data);
    });

    // Update the tile parameter
    client.on('updateTiles', function(tiles) {
        console.log("update tile");
        client.broadcast.emit('updateTiles', tiles);
    });

    client.on('updatePuzzle', function(Puzzle) {
        console.log("updatePuzzle");
        client.broadcast.emit('updatePuzzle', Puzzle);
    });

});

// Start the server (NOTE: user http.listen, NOT app.listen!!)
var server = http.listen(8080, function () {
    console.log('Server listening at http://localhost:%s', server.address().port);
});
