/**
 * Created by kosen on 15/02/2015.
 */
// Create Express app
var express = require('express');
var app = express();

// Get the http server created by Express
var http = require('http').Server(app);
// Load socket.io
var io = require('socket.io')(http);


var puzzle_manager = require("./old/Project_paperJs/server_script/puzzle_colaborative_conductor.js"); //export function from "puzzle_colaborative_conductor.js";

// Serve /
app.get('/', function (req, res) {
    res.sendFile(__dirname + "/old/Project_paperJs/html/puzzle-test.htm");
});

// Serve static pages from the html directory
app.use('/', express.static(__dirname + '/old/Project_paperJs/html'));

// Listen to socket connections
io.on('connect', function(client) {
    //console.log('A user connected');

    var user = 'unknown';

    // Listen to disconnections
    client.on('disconnect', function() {
        puzzle_manager.removeUser(client.id);
        console.log(user, 'disconnected');
        io.emit('bye', user);
    });

    // Listen to client messages
    client.on('hello', function(name) {
        user = name;
        puzzle_manager.addUser(client.id,user);
        //client.emit('settingmsg',puzzle_manager.addUser(client.id,user)); // addUser return the puzzle settings
        console.log(user, 'joined');

        // Welcomme message
        client.emit('msg',{
            from: 'Serveur',
            msg: "Bonjour " + user + ' ( ' + puzzle_manager.numberOfUsers() +'users connected)'});

        // Broadcast to others
        client.broadcast.emit('hello', name);
    });
    client.on('msg', function(data) {
        console.log(data);
        // Broadcast to others
        client.broadcast.emit('msg', data);
    });

    // Update the tile parameter
    client.on('updateTiles', function(tiles) {
        console.log("update tile");
        client.broadcast.emit('updateTiles', tiles);
    });

});

// Start the server (NOTE: user http.listen, NOT app.listen!!)
var server = http.listen(8080, function () {
    console.log('Server listening at http://localhost:%s', server.address().port);
});
