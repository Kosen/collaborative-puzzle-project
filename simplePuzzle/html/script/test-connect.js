/**
 * Created by kosen on 15/02/2015.
 */
var client = null;	// socket connection to server

function connect() {
    //$('#chat').append('<p>Connecting to server</p>');
    client = io();

    // Handler when connected
    client.on('connect', function() {
        //$('#chat').append('<p>Connected to server</p>');
    });

    // Handler when lost connection
    client.on('disconnect', function() {
       // $('#chat').append('<p>Disconnected from server</p>');
    });
}