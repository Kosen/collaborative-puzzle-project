/**
 * Created by kosen on 15/02/2015.
 */

// For calling who look this way :
//require("./server_script/puzzle_colaborative_conductor.js")();
// testTrolo();

//module.exports = function(){
//    this.createApplication = function() {
//        console.log("test createApplication");
//    };
//    this.testTrolo = function() {
//        console.log("testTrolo");
//        this.createApplication();
//        plop();
//    };
//}

// For calling who look this way :
//var puzzle_manager = require("./server_script/puzzle_colaborative_conductor.js");
//puzzle_manager.testTrolo();


var users_List = {}; //Map (associative array) of connected user, the key is the socketID
var color_list = ['green','red','fuchsia','yellow','purple','teal','maroon']; //list of available color

module.exports = {
    numberOfUsers : function() {
        return get_users_List_size ();
    },
    addUser : function(clientID,name) {
        console.log("----------- addUser -----------");
        var currentUser = users_List[clientID] = {name: name, ucolor: assignColor(),clientID : clientID};
        //display_user_property(currentUser);
        get_users_List_size ();
        console.log('-------------------------------');
        return setUserParameter(currentUser);
    },
    removeUser : function(clientID) {
        console.log("---------  removeUser ---------  ");

        console.log("removeUser " + users_List[clientID].name);
        delete users_List[clientID.toString()];
        get_users_List_size ();
        console.log('-------------------------------');
    }
}

function setUserParameter(user){

    return {
        type: 'settings',
        user_color: user.ucolor};
}


function assignColor()
 {
     // WARNING 6 User Max, because 6 colors
     // TODO : Dynamicaly set the color of the user no matter how many user are connected
     var ucolor = color_list[Object.keys(users_List).length];
     /*for (var i = 0; i < color_list.length; i++)
     {

     }*/
     return ucolor;
 }


function display_users_List (){
    console.log("#######  display_users_List #######");
    for ( var key in users_List) {
        if (users_List.hasOwnProperty(key)) {
            console.log('Key  : ' + key);
            display_user_property(key);
        }
    }
    get_users_List_size ();
}

function display_user_property(currentUser) {
    console.log("#######  display_user_property #######");
    //console.log('           Key  : ' + Object.indexOf(currentUser));  // TODO :  Get the index (key) of this array
    for (var property in currentUser) {
        if (currentUser.hasOwnProperty(property)) {
            console.log('           proprietée: ' + property + "  -   " + currentUser[property]);
        }
    }
}

function get_users_List_size (){
    var size = Object.keys(users_List).length; //Object.keys(users_List).length  because is a Map
    console.log("number of user : " + size);
    return size;
}